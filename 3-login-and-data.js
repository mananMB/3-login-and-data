const fsp = require("fs/promises");
const fs = require("fs");
const path = require("path");

// Note: For all file operations use promises with fs
// Q1. Create 2 files simultaneously (without chaining two function calls one after the other).
// Wait for 2 seconds and starts deleting them one after another.
// (Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)
const problem1 = () => {
  const files = ["file1", "file2"];

  const createFiles = () => {
    return Promise.all(
      files.map((file) => {
        return fsp.writeFile(`${__dirname}/${file}`, "");
      })
    );
  };

  const deleteFiles = () => {
    return Promise.all(
      files.map((file) => {
        return fsp.rm(`${__dirname}/${file}`);
      })
    );
  };

  createFiles().catch(console.log);
  setTimeout(() => {
    deleteFiles().catch(console.log);
  }, 2000);
};

problem1();

// Q2. Create a new file with lipsum data (you can google and get this).
// Do File Read and write data to another file
// Delete the original file
//
//     A. using promises chaining
const problem2A = () => {
  fsp
    .readFile(__dirname + "/lipsum.txt", "utf-8")
    .then((data) => {
      return fsp.writeFile(__dirname + "/new-lipsum.txt", data);
    })
    .then(() => {
      return fsp.rm(__dirname + "/lipsum.txt");
    })
    .then(() => {
      console.log("Operation Completed Successfully Using Promises");
    })
    .catch((reject) => {
      console.error("Here: ", reject);
    });
};

//     B. using callbacks
const problem2B = () => {
  fs.readFile(__dirname + "/lipsum.txt", "utf-8", (err, data) => {
    if (err) {
      console.trace(err);
    } else {
      fs.writeFile(__dirname + "/new-cb-lipsum.txt", data, (err) => {
        if (err) {
          console.trace(err);
        } else {
          fs.unlink(__dirname + "/lipsum.txt", (err) => {
            if (err) {
              console.trace(err);
            } else {
              console.log("Operation Completed Successfully Using Callbacks");
            }
          });
        }
      });
    }
  });
};

// Q3.
function login(user, val) {
  if (val % 2 === 0) {
    return Promise.resolve(user);
  } else {
    return Promise.reject(new Error("User not found"));
  }
}

function getData() {
  return Promise.resolve([
    {
      id: 1,
      name: "Test",
    },
    {
      id: 2,
      name: "Test 2",
    },
  ]);
}

function logData(user, activity) {
  return fsp
    .access("user-activity.txt")
    .catch((reject) => {
      if (reject.code === "ENOENT") {
        return fsp.writeFile("user-activity.txt", "{}");
      }
    })
    .then(() => {
      return fsp.readFile("user-activity.txt", "utf-8");
    })
    .then((res) => {
      let userActivityData = JSON.parse(res);
      if (!(user in userActivityData)) {
        userActivityData[user] = { activities: [] };
      }
      userActivityData[user].activities.push({
        activity: activity,
        timestamp: new Date(Date.now()),
      });
      return fsp.writeFile(
        "user-activity.txt",
        JSON.stringify(userActivityData, null, 4)
      );
    })
    .catch((reject) => {
      console.log(reject);
    });
}

// Use appropriate methods to
// A. login with value 3 and call getData once login is successful
const user = "Manan";
const val = 2;

const problem3A = () => {
  login(user, val)
    .then(() => {
      return getData();
    })
    .then((data) => {
      console.log(data);
    })
    .catch((rej) => {
      console.log(rej);
    });
};

// B. Write logic to logData after each activity for each user. Following are the list of activities
//     "Login Success"
//     "Login Failure"
//     "GetData Success"
//     "GetData Failure"
//
//     Call log data function after each activity to log that activity in the file.
//     All logged activity must also include the timestamp for that activity.
// login(user, 2).then(() => {});
//     You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
//     All calls must be chained unless mentioned otherwise.

const loginSuccess = "Login Success";
const loginFailure = "Login Failure";

const getDataSuccess = "GetData Success";
const getDataFailure = "GetData Failure";

const problem3B = () => {
  login(user, val)
    .then(() => {
      return logData(user, loginSuccess);
    })
    .then((res) => {
      return getData();
    })
    .then(() => {
      return logData(user, getDataSuccess);
    })
    .catch((rej) => {
      if (rej.message === "User not found") {
        return logData(user, loginFailure);
      }
      return logData(user, getDataFailure);
    });
};
